--[[
FishBus Gaming permissions system - Utility functionality

Shamelessly originating from ExplosiveGamings's mod.
This is the 'common use' functionality
--]]

function ticktohour (tick)
    local hour = tostring(math.floor(tick/(216000*game.speed)))
    return hour
end

function ticktominutes (tick)
  	local minutes = math.floor(tick/(3600*game.speed))
    return minutes
end

function clearSelection(player)
	global.selected[player.index] = {}
end
