--[[
FishBus Gaming permissions system - Server Information GUI

Shamelessly originating from ExplosiveGamings's mod.
permissions are based on ranks.
--]]

require 'event'
require 'mod-gui'

require 'fb_ranks'
require 'fb_util'
require 'fb_util_gui'
require 'fb_util_playertable'

local frameName = 'Info'
local frameCaption = 'ServInfo'

fbgui.createFrame(frameName, 6, 'Rules', frameCaption, 'Rules, Server info, How to chat, Playerlist, Adminlist.')

fbgui.createTab(frameName, 'Rules', 'The rules of the server', function(player,frame)
	local rules = {
		"Do not disrespect any player in the server (This includes staff).",
		"Do not spam. This includes chat spam, item spam, chest spam etc.",
		"Do not lay down concrete with bots without permission.",
		"Do not use active provider chests without permission.",
		"Do not use speakers on global without permission.",
		"Do not remove/move major parts of the factory without permission.",
		"Do not walk in player random direction for no reason (to save map size).",
		"Do not remove stuff just because you don't like it, tell people first.",
		"Do not make train roundabouts.",
		--"Trains are Left Hand Drive (LHD) only.",
		"Do not complain about lag, low fps and low ups or other things like that.",
		"Do not ask for rank.",
		"What an admin says, goes.",
	}
	for i, rule in pairs(rules) do
		frame.add{name=i, type="label", caption={"", i ,". ", rule}}
	end
end)
fbgui.createTab(frameName, 'Community', 'Info about the community', function(player,frame)
	frame.add{name=1, type="label", caption="Our community news, chat & help is completely on Discord.  You can join us at:"}
	frame.add{name=2, type='textfield', text='https://discord.gg/vdwxNUR'}.style.minimal_width=400
	frame.add{name=3, type="label", caption={"", "Our website(incomplete):"}}
	frame.add{name=4, type='textfield', text='http://fish-bus.com'}.style.minimal_width=400
	--frame.add{name=5, type="label", caption={"", "Steam:"}}
	--frame.add{name=6, type='textfield', text='http://steamcommunity.com/groups/tntexplosivegaming'}.style.minimal_width=400
end)
fbgui.createTab(frameName, 'How to chat', 'Just in case you dont know how to chat', function(player,frame)
	local chat = "Chatting for new players can be difficult because it’s different than other games! It’s very simple, the button you need to press is the “GRAVE ` / TILDE ~” key it’s located under the “ESC key”. If you would like to change the key go to your controls tab in options. The key you need to change is “Toggle Lua console” it’s located in the second column 2nd from bottom."
	frame.add{name=i, type="label", caption={"", chat}, single_line=false}.style.maximal_width=530
end)

fbgui.createTab(frameName, 'Mod Info', 'Info about the various softmods we use on this server', function(player,frame)
	local info = {
		"Note: Softmods are server-side scripts and will not disable achievements.",
		"Fish Market will randomly grant you fish based on mining/chopping/rock removal",
		"Spend your fish on various items at the fish market!",
		"Autofill will add fuel or ammo from your inventory to machines that use said resources",
		"Ignore the fact it mentions \"No Rocket Fuel\""
	}
	for i, line in pairs(info) do
		frame.add{name=i, type="label", caption={"", line}, single_line=false}
	end
end)

fbgui.createTab(frameName, 'Admins', 'List of all the current server admins', function(player,frame)
	local admins = {
		"This list contains all the people that are admin on this server.",
		"Do not ask to become an admin!",
		"Our admins periodically will promote active & useful players.",
	}
	for i, line in pairs(admins) do
		frame.add{name=i, type="label", caption={"", line}, single_line=false}
	end
	drawPlayerTable(player, frame, false, false, {'admin'})
end)
fbgui.createTab(frameName, 'Players', 'List of all the people who have been on the server', function(player,frame)
	local players = {
		"These are the players who have supported us in the making of this factory.",
	}
	for i, line in pairs(players) do
		frame.add{name=i, type="label", caption={"", line}}
	end
	frame.add{name='filterTable',type='table',colspan=3}
	frame.filterTable.add{name='name_label',type='label',caption='Name'}
	frame.filterTable.add{name='status_label',type='label',caption='Online?'}
	frame.filterTable.add{name='hours_label',type='label',caption='Online Time (minutes)'}
	frame.filterTable.add{name='name_input',type='textfield'}
	frame.filterTable.add{name='status_input',type='textfield'}
	frame.filterTable.add{name='hours_input',type='textfield'}
	drawPlayerTable(player, frame, false, false, {})
end)

Event.register(defines.events.on_player_joined_game, function(event)
	local player = game.players[event.player_index]
	local myFrame = fbgui.getFrame(frameName)[1]
	
	-- create button of specific name.  That gets it to link up to the action
	-- automatically defined by createFrame(...)
	if remote.call('fb_ranks', 'getRank', player).power <= myFrame.require then
		fbgui.createToolbarButton(player, "btn_"..frameName, myFrame.caption, myFrame.tooltip, myFrame.sprite)
	end
	-- automatically pop up the Info pane to new users.
    if not player.admin and ticktominutes(player.online_time) < 1 then
		fbgui.drawFrame(player, frameName, 'Rules')
	end
end)
