--[[
FishBus Gaming permissions system - Player List (with rank/time played)

Shamelessly originating from ExplosiveGamings's mod.
permissions are based on ranks.
--]]

require 'event'
require 'mod-gui'

require 'fb_ranks'
require 'fb_util'
require 'fb_util_gui'

local function drawPlayerList(player)
	for i, player in pairs(game.connected_players) do
		local flow = mod_gui.get_frame_flow(player)
		if  flow.PlayerList == nil then
			flow.add{type = "frame", name= "PlayerList", direction = "vertical",style=mod_gui.frame_style}
			    .add{type = "scroll-pane", name= "PlayerListScroll", direction = "vertical", vertical_scroll_policy="always", horizontal_scroll_policy="never"}
		end
		local Plist = flow.PlayerList.PlayerListScroll
		Plist.clear()
		Plist.style.maximal_height = 200
		for i, player in pairs(game.connected_players) do
			playerRank = remote.call('fb_ranks', 'getRank', player)
			if playerRank.power <= 3 or playerRank.name == 'Jail' then
				if playerRank.shortHand ~= '' then
					Plist.add{type = "label",  name=player.name, style="caption_label_style", caption={"", ticktohour(player.online_time), " H - " , player.name..' - '..playerRank.shortHand}}
				else
					Plist.add{type = "label",  name=player.name, style="caption_label_style", caption={"", ticktohour(player.online_time), " H - " , player.name}}
				end
				Plist[player.name].style.font_color = playerRank.colour
				--player.tag = playerRank.tag
			end
		end
		for i, player in pairs(game.connected_players) do
			playerRank = remote.call('fb_ranks', 'getRank', player)
			if playerRank.power > 3 and playerRank.name ~= 'Jail' then
				if playerRank.shortHand ~= '' then
					Plist.add{type = "label",  name=player.name, style="caption_label_style", caption={"", ticktohour(player.online_time), " H - " , player.name..' - '..playerRank.shortHand}}
				else
					Plist.add{type = "label",  name=player.name, style="caption_label_style", caption={"", ticktohour(player.online_time), " H - " , player.name}}
				end
				Plist[player.name].style.font_color = playerRank.colour
				--player.tag = playerRank.tag
			end
		end
	end
end

local function drawPlayerListButton(player)
	local frame = mod_gui.get_button_flow(player)
	if not frame.btn_toolbar_playerList then
		fbgui.createButton(frame, "btn_toolbar_playerList", "Playerlist", "Adds/removes the player list to/from your game.",'entity/player')
	end
end

Event.register(defines.events.on_player_joined_game, function(event)
	local player = game.players[event.player_index]
	drawPlayerList(player)
	drawPlayerListButton(player)
end)

Event.register(defines.events.on_player_respawned, function(event)
	local player = game.players[event.player_index]
	drawPlayerList(player)
end)

Event.register(defines.events.on_player_left_game, function(event)
	local player = game.players[event.player_index]
	drawPlayerList(player)
end)

Event.register(defines.events.on_gui_click, function(event)
	if not (event and event.element and event.element.valid) then return end
	
	if(event.element.type == 'sprite-button' or event.element.type == 'button') then
		if(event.element.name == 'btn_toolbar_playerList') then
			local player = game.players[event.player_index]
			fbgui.toggleVisible(mod_gui.get_frame_flow(player).PlayerList)
		end
	end
end)

Event.register("custom.fb_rankchange", function (event)
	local player = event.player
	drawPlayerList(player)
end)

