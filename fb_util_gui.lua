--[[
FishBus Gaming permissions system - GUI handling functionality

Shamelessly originating from ExplosiveGamings's mod.
permissions are based on ranks.

Updates:
- Create it as a module.
- Add functionality to remove a toolbar button.
--]]

require 'mod-gui'
require 'event'

fbgui = {}

fbgui.guis = {frames={},buttons={}}

local openTab

-- EG's addFrame
function fbgui.createFrame(frame, rank, open, caption, tooltip, sprite)
	fbgui.guis.frames[frame] = {{
		require=rank, caption=caption, tooltip=tooltip, sprite=sprite
	}}
	fbgui.connectButton('close', function(player,element)
		element.parent.parent.parent.destroy()
	end)
	fbgui.connectButton('btn_'..frame, function(player,element)
		if player.gui.center[frame] then
			player.gui.center[frame].destroy()
		else
			fbgui.drawFrame(player,frame,open)
		end
	end)
end

function fbgui.getFrame(name)
	return fbgui.guis.frames[name]
end

-- EG's addTab
function fbgui.createTab(frame, tabName, describtion, drawTab)
	fbgui.guis.frames[frame][tabName] = {tabName, describtion, drawTab}
	fbgui.connectButton(tabName, function(player, element)
		openTab(player, element.parent.parent.parent.name, element.parent.parent.parent.tab, element.name)
	end)
end

-- EG's addButton
function fbgui.connectButton(buttonName, onClick)
	fbgui.guis.buttons[buttonName] = {buttonName, onClick}
end

-- EG's fbgui.drawButton
function fbgui.createButton(frame, buttonName, caption, description, sprite)
	if sprite then
		frame.add{name=buttonName, type = "sprite-button", sprite=sprite, tooltip=description, style = mod_gui.button_style}
	else
		frame.add{name=buttonName, type = "button", caption=caption, tooltip=description, style = mod_gui.button_style}
	end
end

function fbgui.removeToolbarButton(player, buttonName)
	local frame = mod_gui.get_button_flow(player)
	if frame[buttonName] then
		frame[buttonName].destroy()
	end
end

function fbgui.createToolbarButton(player, buttonName, caption, description, sprite)
	local frame = mod_gui.get_button_flow(player)
	if frame[buttonName] then
		frame[buttonName].destroy()
	end
	if not frame[buttonName] then
		fbgui.createButton(frame, buttonName, caption, description, sprite)
	end
end

function openTab(player, frameName, tab, tabName)
	local tabBar = player.gui.center[frameName].tabBarScroll.tabBar
	for _,t in pairs(fbgui.guis.frames[frameName]) do
		if _ ~= 1 then
			if t[1] == tabName then
				tabBar[t[1]].style.font_color = {r = 255, g = 255, b = 255, player = 255}
				tab.clear()
				t[3](player, tab)
			else
				tabBar[t[1]].style.font_color = {r = 100, g = 100, b = 100, player = 255}
			end
		end
	end
end

function fbgui.drawFrame(player, frameName, tabName)
	p_rank = remote.call("fb_ranks", 'getRank', player)
	if p_rank.power <= fbgui.guis.frames[frameName][1].require then
		player.gui.center.clear()
		local frame = player.gui.center.add{name=frameName,type='frame',caption=frameName,direction='vertical',style=mod_gui.frame_style}
		local tabBarScroll = frame.add{type = "scroll-pane", name= "tabBarScroll", vertical_scroll_policy="never", horizontal_scroll_policy="always"}
		local tabBar = tabBarScroll.add{type='flow',direction='horizontal',name='tabBar'}
		local tab = frame.add{type = "scroll-pane", name= "tab", vertical_scroll_policy="auto", horizontal_scroll_policy="never"}
		for _,t in pairs(fbgui.guis.frames[frameName]) do
			if _ ~= 1 then fbgui.createButton(tabBar, t[1], t[1], t[2]) end
		end
		openTab(player, frameName, tab, tabName)
		fbgui.createButton(tabBar, 'close', 'Close', 'Close this window')
		tab.style.minimal_height = 300
		tab.style.maximal_height = 300
		tab.style.minimal_width = 550
		tab.style.maximal_width = 550
		tabBarScroll.style.minimal_height = 60
		tabBarScroll.style.maximal_height = 60
		tabBarScroll.style.minimal_width = 550
		tabBarScroll.style.maximal_width = 550
		player.gui.center.add{type='frame',name='temp'}.destroy()
	end
end

function fbgui.toggleVisible(frame)
	if frame then
		if frame.style.visible == nil then
			frame.style.visible = false
		else
			frame.style.visible = not frame.style.visible
		end
	end
end

-- Events associated with gui management.

Event.register(defines.events.on_gui_click, function(event)
	if not (event and event.element and event.element.valid) then return end
	
	if event.element.type == 'button' or event.element.type == 'sprite-button' then
		for _,btn in pairs(fbgui.guis.buttons) do
			if btn[1] == event.element.name then
				if btn[2] then
					local player = game.players[event.player_index]
					btn[2](player, event.element) 
				else 
					remote.call('fb_rank', 'callRank', 'Invaid Button'..btn[1], 'Mod') 
				end
				break
			end
		end
	elseif event.element.type == 'checkbox' then
		if event.element.name == 'select' then
			global.selected[event.player_index] = global.selected[event.player_index] or {}
			selected = global.selected[event.player_index]
			if event.element.state then
				table.insert(selected,event.element.parent.name)
			else
				for _,name in pairs(selected) do
					if name == event.element.parent.name then
						table.remove(selected,_)
						break
					end
				end
			end
		end
	end
end)
