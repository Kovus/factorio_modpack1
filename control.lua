require 'util' -- Factorio-provided

require "builtin-silo"
require "builtin-playerspawn"

require "autodeconstruct"
require "autofill"
require "band"
require "fish_market"
--require 'show-health' -- Do not load, for now - Kov.
require 'fb_score' -- kill counter in addition to rocket score.
require 'fb_player_list'
require 'fb_info_gui'
require 'fb_admin_gui'
require 'fb_adminplus_gui'

require 'fb_antigriefing'
require 'fb_automessage'

local version = 1

Event.register(Event.core_events.init, function()
	global.version = version
end)

Event.register(Event.core_events.configuration_changed, function(event)
	if global.version ~= version then
		global.version = version
	end
end)